#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use defmt::{info, debug, panic};
use embassy_executor::Spawner;
use embassy_time::{Timer, Duration};
use embassy_rp::pac as pac;
use embassy_futures::join::join;
use embassy_rp::interrupt;
use embassy_rp::gpio::{Flex, Pull, Pin};
use embassy_rp::usb::{Driver, Instance};
use embassy_usb::class::cdc_acm::{CdcAcmClass, State};
use embassy_usb::driver::EndpointError;
use embassy_usb::{Builder, Config};
use {defmt_rtt as _, panic_probe as _};

use cortex_m::peripheral::SYST;
use core::fmt::Write;

const MAX_CDC_PACKET: u8 = 64;

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    info!("Hello there!");

    let p = embassy_rp::init(Default::default());

    let mut cp = cortex_m::peripheral::Peripherals::take().unwrap();

    // Create the USB driver, from the HAL.
    let irq = interrupt::take!(USBCTRL_IRQ);
    let driver = Driver::new(p.USB, irq);

    // Create embassy-usb Config
    let mut config = Config::new(0xc0de, 0xcafe);
    config.manufacturer = Some("Embassy");
    config.product = Some("USB-serial example");
    config.serial_number = Some("12345678");
    config.max_power = 100;
    config.max_packet_size_0 = 64;

    // Required for windows compatiblity.
    // https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.9.1/kconfig/CONFIG_CDC_ACM_IAD.html#help
    config.device_class = 0xEF;
    config.device_sub_class = 0x02;
    config.device_protocol = 0x01;
    config.composite_with_iads = true;

    // Create embassy-usb DeviceBuilder using the driver and config.
    // It needs some buffers for building the descriptors.
    let mut device_descriptor = [0; 256];
    let mut config_descriptor = [0; 256];
    let mut bos_descriptor = [0; 256];
    let mut control_buf = [0; 64];

    let mut state = State::new();

    let mut builder = Builder::new(
        driver,
        config,
        &mut device_descriptor,
        &mut config_descriptor,
        &mut bos_descriptor,
        &mut control_buf,
        None,
    );

    // Create classes on the builder.
    let mut class = CdcAcmClass::new(&mut builder, &mut state, 64);

    // Build the builder.
    let mut usb = builder.build();

    // Run the USB device.
    let usb_fut = usb.run();

    // XXX: Can we get the number from the pin?
    // let mut gpio = (Flex::new(p.PIN_6), 6);
    let mut gpio = (Flex::new(p.PIN_10), 10);
    // let mut gpio = (Flex::new(p.PIN_13), 13);

    let usb = false;
    if usb {
        // // Do stuff with the class!
        let echo_fut = async {
            loop {
                class.wait_connection().await;
                info!("Connected");
                let _ = usb_run(&mut gpio.0, gpio.1, &mut cp.SYST, &mut class).await;
                info!("Disconnected");
            }
        };


        // Run everything concurrently.
        // If we had made everything `'static` above instead, we could do this using separate tasks instead.
        join(usb_fut, echo_fut).await;
    } else {
        run(&mut gpio.0, gpio.1, &mut cp.SYST);
        // run2()
    }
}

struct Disconnected {}

impl From<EndpointError> for Disconnected {
    fn from(val: EndpointError) -> Self {
        match val {
            EndpointError::BufferOverflow => panic!("Buffer overflow"),
            EndpointError::Disabled => Disconnected {},
        }
    }
}

#[derive(Default)]
struct FmtBuf {
    s: heapless::String<1024>,
}

impl FmtBuf {
    fn str(&self) -> &str {
        self.s.as_str()
    }
    async fn write<'d, T: Instance+'d>(cdc: &mut CdcAcmClass<'d, Driver<'d, T>>, args: core::fmt::Arguments<'_>) -> Result<(), core::fmt::Error> {
        let mut f = Self::default();
        f.write_fmt(args)?;

        let b = f.str().as_bytes();
        for c in b.chunks(MAX_CDC_PACKET as usize) {
            cdc.write_packet(c).await.map_err(|_| core::fmt::Error)?
        }
        Ok(())
    }
}

impl core::fmt::Write for FmtBuf {
    fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
        self.s.push_str(s).map_err(|_| core::fmt::Error)
    }
}

async fn usb_run<'d, T: Instance + 'd, P: Pin>(pin: &mut Flex<'d, P>, pin_num: usize, syst: &mut SYST,
    class: &mut CdcAcmClass<'d, Driver<'d, T>>) -> Result<(), Disconnected> {
    todo!("impl me")
}

fn run2() {
    loop {
        for i in 100_000..1_000_000 {
            info!("{}", i);
            cortex_m::asm::delay(i);
        }
    }

}

fn run<'d, P: Pin>(pin: &mut Flex<'d, P>, pin_num: usize, syst: &mut SYST) -> Result<(), Disconnected> {
    info!("hello");

    syst.set_reload(10_000_000-1);
    syst.clear_current();
    syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
    syst.enable_counter();

    // Seemed to reduce noise (for characterising capacitor jitter).
    // XXX Somehow disabling ROSC breaks SWD, perhaps signal integrity issues?
    // unsafe{ pac::ROSC.ctrl().modify(|s| s.set_enable(pac::rosc::vals::Enable::DISABLE)) };

    // TODO: test with/without schmitt disable
    unsafe{ pac::PADS_BANK0.gpio(pin_num).modify(|s| s.set_schmitt(false)) };

    // get it near the threshold
    let del = critical_section::with(|_cs| {
        pin.set_as_output();
        pin.set_high();
        // // approx 1ms
        // cortex_m::asm::delay(125_000);
        cortex_m::asm::delay(1200);
        pin.set_as_input();
        pin.set_pull(Pull::Down);
        syst.clear_current();
        let t1 = SYST::get_current();
        // Get it near the threshold to begin.
        while pin.is_high() {}
        let t2 = SYST::get_current();
        t1 - t2
    });
    info!("initial pulldown del is {}", del);

    let mut up_x = 1u32;
    let mut down_x = 1u32;
    /*
                                           _ still pullup, =up_x
           t3      t4       t1      t2    /
   |prev    |pulldn |still  | pullup|    /
   |iter    |active |pulldn,| active|   / | pulldn active, etc
   |        |=t_down|=down_x| =t_up |  o  | measuring next t_down, etc...
   |        .       |       |       |     |
          .   .     |       |       |     .
        .       .   |       |       |   .   .
      .           . |       |       | .       .
    .---------------.---------------.-----------.------ GPIO threshold. Probably isn't really flat.
                      .           .               .
                        .       .
                          .   .
                            .

    The GPIO pin is attached to a capacitor to ground. 0.1uF used for experiment.

    The GPIO pin is left as input and is driven low/high with pulldown/pullup
    resistors. The time taken to reach logical low/high is measured. The pulldown/pullup
    resistor is left on for a further time down_x/up_x, which intends to drive
    the voltage further so that a useful time can be measured for the next t_up/t_down
    cycle.

    The down_x/up_x overshoot cycle times are doubled each iteration and add in the measured time,
    amplifying noise captured in t_down/t_up measurements.

    down_x/up_x time is kept within a sensible range with modulo.

    The random output is t_down and t_up time.
    */

    /* Experimental values:
    0.1uF "monolithic" perhaps from futurlec?
    GPIO6
    0.003717 INFO  initial pulldown del is 346730
    0.003710 INFO  initial pulldown del is 345770
    less than ~13000 overshoot had lots of no-delays

    Altronics R8617 • 0.01uF 50V Y5V 0805 SMD Chip Capacitor PK 10
    GPIO10
    0.001292 INFO  initial pulldown del is 43234
    0.001294 INFO  initial pulldown del is 43598


    Altronics R8629 • 0.047uF 50V X7R 0805 SMD Chip Capacitor PK 10
    GPIO13
    0.002497 INFO  initial pulldown del is 193878
    0.002547 INFO  initial pulldown del is 200170


    */


    loop {
        // number of iterations per interrupt-free block
        const LOOP: usize = 2000;

        // Range of cycle counts for overshoot. Upper limit is the pullup/pulldown time
        // from vcc/gnd to threshold.
        // A large upper limit will also decrease the number of samples/second (but will
        // perhaps have more entropy bits/sample?)
        // Lower limit is necessary because we don't want to hit the threshold immediately
        // on reading.
        // Empirical values for a 1uF capacitor.
        // TODO this could be determined from a calibration step?
        // const UPRANGE: u32 = 50000;
        // const LOWRANGE: u32 = 15000;
        // nice integers
        const LOW_OVER: u32 = 1000;
        const HIGH_OVER: u32 = LOW_OVER + 16384;

        // for debug printout
        // BUF iterations from the start (or end?) will be printed. must be <LOOP
        const BUF: usize = 64;
        // for end of buffer
        // const BUF_START: usize = LOOP - BUF;
        // for start of buffer
        const BUF_START: usize = 0;
        let mut ups = [0u32; BUF];
        let mut downs = [0u32; BUF];
        let mut upx = [0u32; BUF];
        let mut downx = [0u32; BUF];

        // The main iteration function for experimentation.
        // It calculates the next overshoot #cycles.
        // `prev` is the #cycles returned from last step(), `meas` is the time
        // taken for the return to centre value (which is targetting `T`).
        // eg `up_x = step(up_x, t_down);`
        let step = |prev: u32, meas: u32| {
            // Doubling step.
            // x[n] = 2*x[n-1] + meas
            let v = prev*2 + meas;

            // modulo to sensible range
            if v > HIGH_OVER {
                LOW_OVER + (v % (HIGH_OVER - LOW_OVER))
            } else {
                v
            }
        };

        critical_section::with(|_cs| {

            // We alternate between taking the measured down/up values as real output,
            // and using it to compute the next overshoot.
            // That prevents leaking "real output" values in the timing of subsequent overshoot
            // delays.
            for i in 0..LOOP {
                pin.set_pull(Pull::Up);
                syst.clear_current();
                let t1 = SYST::get_current();
                let imm = pin.is_high();
                while pin.is_low() {}
                let t2 = SYST::get_current();
                let t_up = t1 - t2;
                if i % 2 == 0 {
                    down_x = step(down_x, t_up);
                } else {
                    if i > 4 {
                        info!("t_up {} {} from down_x {}", t_up, imm, down_x);
                        // TODO: keep `t_up` as our random output
                    }
                    // only for nodouble testing
                    // down_x = step(down_x, t_up);
                }
                cortex_m::asm::delay(up_x);

                pin.set_pull(Pull::Down);
                syst.clear_current();
                let t3 = SYST::get_current();
                let imm = pin.is_low();
                while pin.is_high() {}
                let t4 = SYST::get_current();
                let t_down = t3 - t4;
                if i % 2 == 0 {
                    up_x = step(up_x, t_down);
                } else {
                    if i > 4 {
                        info!("t_down {} {} from up_x {}", t_down, imm, up_x);
                        // TODO: keep t_down
                    }
                    // only for nodouble testing
                    // up_x = step(up_x, t_down);
                }
                cortex_m::asm::delay(down_x);

                if (BUF_START..BUF_START+BUF).contains(&i) {
                    let n = i - BUF_START;
                    ups[n] = t_up;
                    upx[n] = up_x;
                    downs[n] = t_down;
                    downx[n] = down_x;
                }

            }
            // put it in idle state so it'll be closer to threshold
            // on the next critical_section iteration.
            pin.set_pull(Pull::None);
        });

        // for i in 0..BUF-1 {
        //     if i % 2 == 1 {
        //         info!("U {},{} x {}  D {},{} x {} use", ups[i], ups[i+1], upx[i], downs[i], downs[i+1], downx[i]);
        //     } else {
        //     }
        // }
        // info!("===");
    }
}
